require 'minitest/autorun'
require 'minitest/spec'

module RefFixnum
  refine Fixnum do
    alias_method :old_add, :+

    def +(other)
      other * 0
    end
  end
end

class RefTest
  using RefFixnum

  def add(a, b)
    a + b
  end
end

describe 'Refinements' do
  it { RefTest.new.add(34, 56).must_equal(0) }
  it { (34 + 56).must_equal(90) }
end
