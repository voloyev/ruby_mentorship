require 'benchmark'
require './searches'
# ruby 2.6

arr = (1..).step(5).take(1000000)

key = 1000

Benchmark.bm do |x|
  x.report('linear') { linear_search(arr, key) }
  x.report('binary') { binary_search(arr, key) }
end
