class Tree
  attr_reader :name

  def initialize(name)
    @name = name
  end
end

class TreeFactory
  attr_reader :pool
  def initialize
    @pool = {}
  end

  def find_tree(name)
    # if the tree already exists, reference it instead of creating a new one
    if @pool.key?(name)
      tree = @pool[name]
    else
      tree = create_tree(name)
      @pool[name] = tree
    end
    tree
  end

  def pool_size
    @pool.size
  end

  private

  def create_tree(tree)
    Tree.new(tree)
  end
end

class Wood
  attr_reader :tree_factory

  def initialize
    @tree_factory = TreeFactory.new
    @wood = []
  end

  def add_tree(tree)
    tree = @tree_factory.find_tree(tree)
    @wood << tree
  end

  def pool
    @tree_factory.pool
  end
  
  def result
    @wood.map(&:name)
  end
end

wood = Wood.new
trees = ["oak", "baobab", "oak", "baobab"]

p 'Creation'
p trees.map(&:object_id)

trees.each do |tree|
  wood.add_tree(tree)
end

p 'After'
p wood.result
p wood.tree_factory.pool_size
p wood.pool
