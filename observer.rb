class Publisher
  attr_reader :message

  def initialize
    @subscribers = []
  end

  def message=(new_message)
    @message = new_message
    notify
  end

  def create_subscription(subscriber)
    @subscribers.push(subscriber)
  end

  def cancel_subscription(subscriber)
    @subscribers.remove(subscriber)
  end

  private

  def notify
    @subscribers.each { |subscriber| subscriber.update(message) }
  end
end

class Subscriber
  def update(changes)
    puts "Hey! Here are changes: #{changes}"
  end
end

subscriber_first = Subscriber.new
subscriber_second = Subscriber.new
publisher = Publisher.new

publisher.create_subscription(subscriber_first)
publisher.create_subscription(subscriber_second)

# Let's give the subject a name
publisher.message = 'Hello!'
publisher.message = 'New message'
