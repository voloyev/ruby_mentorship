describe 'Practice' do
  describe 'PredicateMatchersExample' do
    class PredicateMatchersExample
      def has_predicate?
        true
      end
    end

    it 'have predicate' do
      predicate = PredicateMatchersExample.new
      expect(predicate).to have_predicate
    end

    describe 'all matchers' do
      it { expect([3,7,9]).to all(be_odd) }
    end

    describe 'change matchers' do
      context 'array' do
        it 'change aray size' do
          list = [1,2,3,4]
          expect {list << 5}.to change(list, :size).from(4).to(5)
        end
      end
    end
  end
end
