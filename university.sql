BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "students" (
	"id"	integer NOT NULL,
	"first_name"	TEXT,
	"last_name"	TEXT,
	"faculty_id"	integer,
	"phone"	integer,
	FOREIGN KEY("faculty_id") REFERENCES "faculties"("id"),
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "faculties" (
	"id"	integer NOT NULL,
	"name"	text,
	"phone"	integer,
	PRIMARY KEY("id")
);
INSERT INTO "students" VALUES (1,'bob','anderson',1,911);
INSERT INTO "students" VALUES (2,'Ann','Second',2,102);
INSERT INTO "faculties" VALUES (1,'math',321311);
INSERT INTO "faculties" VALUES (2,'biology',567112);
CREATE VIEW students_info as
SELECT st.id AS student_id, 
st.first_name || ' ' || st.last_name AS full_name, 
st.phone as student_phone,
f.name as faculty_name,
f.phone as faculty_phone
FROM students st
INNER JOIN faculties as f on f.id = st.id;
CREATE VIEW student_master as
SELECT st.id AS student_id, 
st.first_name || ' ' || st.last_name AS name, 
st.phone,
f.name as faculty_name
FROM students st
INNER JOIN faculties as f on f.id = st.id;
COMMIT;
