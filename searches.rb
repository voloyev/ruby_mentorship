def linear_search(array, key)
  if array.index(key).nil?
    return -1
  else
    return "#{key} at index #{array.index(key)}"
  end
end

def binary_search(array, key)
  low, high = 0, array.length - 1
  while low <= high
    mid = (low + high) >> 1
    case key <=> array[mid]
    when 1
      low = mid + 1
    when -1
      high = mid - 1
    else
      return mid
    end
  end
end
