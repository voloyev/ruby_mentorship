    class Sheep
      def initialize(params={})
         @name = params.fetch(:name, nil)
         @codename = params.fetch(:codename, nil)
      end

      def _clone(codename)
        Sheep.new(
          codename: self.codename(codename),
          name: self.name
        )
      end

      def codename(codename)
        codename ? @codename = codename : codename
      end

      def name
        "Betty"
      end
    end

    sheep_prototype = Sheep.new
    sheep_instance1 = sheep_prototype._clone("007")
    sheep_instance2 = sheep_prototype._clone("3312")

    p sheep_instance1 #<Sheep:0x000055b2aac250d0 @name="Betty", @codename="007">
    p sheep_instance2 #<Sheep:0x000055b2aac25030 @name="Betty", @codename="3312">

